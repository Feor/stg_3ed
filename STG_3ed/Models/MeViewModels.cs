﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace STG_3ed.Models
{
    // Модели, возвращенные действиями MeController.
    public class GetViewModel
    {
        public string Hometown { get; set; }
    }
}